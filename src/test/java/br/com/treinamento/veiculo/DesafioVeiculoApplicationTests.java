package br.com.treinamento.veiculo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DesafioVeiculoApplicationTests {

	@Test
	void Carro() {
		Carro carro = new Carro("Fiat", "Argo", "Preto", 50000.00, 4, 25.00 );
		carro.abastecer();
		carro.acelerar();
		carro.ligar();

		System.out.println(carro.toString());
	}

	@Test
	void Caminhao(){
		Caminhao caminhao = new Caminhao("Volvo", "VOLVO FH540", "Cinza", 500000.00, 200.00, 50.00);
		caminhao.abastecer();
		caminhao.acelerar();
		caminhao.carregar(300.00);
		System.out.println(caminhao.toString());
	}
}
