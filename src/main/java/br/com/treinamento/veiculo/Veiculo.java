package br.com.treinamento.veiculo;

public abstract class Veiculo {
  private String marca;
  private String modelo;
  private String cor;
  private Double preco;

  public Veiculo() {}

  public Veiculo(String marca, String modelo, String cor, Double preco) {
    this.marca = marca;
    this.modelo = modelo;
    this.cor = cor;
    this.preco = preco;
  }

  public String getMarca() {
    return marca;
  }

  public void setMarca(String marca) {
    this.marca = marca;
  }

  public String getModelo() {
    return modelo;
  }

  public void setModelo(String modelo) {
    this.modelo = modelo;
  }

  public String getCor() {
    return cor;
  }

  public void setCor(String cor) {
    this.cor = cor;
  }

  public Double getPreco() {
    return preco;
  }

  public void setPreco(Double preco) {
    this.preco = preco;
  }

  public void abastecer() {}

  public void acelerar() {}

  @Override
  public String toString() {
    return "========= Veiculo ========="
        + "Marca:'"
        + marca
        + '\''
        + ", Modelo='"
        + modelo
        + '\''
        + ", Cor='"
        + cor
        + '\''
        + ", Preço="
        + preco;
  }
}
