package br.com.treinamento.veiculo;

public class Carro extends Veiculo {
  private Integer numeroPorta;
  private Double potencia;

  public Carro(Integer numeroPorta, Double potencia) {}

  public Carro(
      String marca, String modelo, String cor, Double preco, Integer numeroPorta, Double potencia) {
    super(marca, modelo, cor, preco);
    this.numeroPorta = numeroPorta;
    this.potencia = potencia;
  }

  public Integer getNumeroPorta() {
    return numeroPorta;
  }

  public void setNumeroPorta(Integer numeroPorta) {
    this.numeroPorta = numeroPorta;
  }

  public Double getPotencia() {
    return potencia;
  }

  public void setPotencia(Double potencia) {
    this.potencia = potencia;
  }

  public void ligar() {
    System.out.println("Carro ligado!");
  }

  @Override
  public void abastecer() {
    System.out.println("Carro abastecido!");
  }

  @Override
  public void acelerar() {
    System.out.println("Carro acelerando...");
  }

  @Override
  public String toString() {
    return "========= Carro ========="
        + "\n"
        + "Marca:'"
        + getMarca()
        + "\n"
        + "Modelo:'"
        + getModelo()
        + "\n"
        + "Cor:'"
        + getCor()
        + "\n"
        + "Preco:"
        + getPreco()
        + "\n"
        + "NumeroPorta:"
        + numeroPorta
        + "\n"
        + "Potencia:"
        + potencia
        + "\n";
  }
}
