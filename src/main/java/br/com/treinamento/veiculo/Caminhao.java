package br.com.treinamento.veiculo;

public class Caminhao extends Veiculo {
  private Double capacidadeCarga;
  private Double torque;

  public Caminhao() {}

  public Caminhao(
      String marca,
      String modelo,
      String cor,
      Double preco,
      Double capacidadeCarga,
      Double torque) {
    super(marca, modelo, cor, preco);
    this.capacidadeCarga = capacidadeCarga;
    this.torque = torque;
  }

  public Double getCapacidadeCarga() {
    return capacidadeCarga;
  }

  public void setCapacidadeCarga(Double capacidadeCarga) {
    this.capacidadeCarga = capacidadeCarga;
  }

  public Double getTorque() {
    return torque;
  }

  public void setTorque(Double torque) {
    this.torque = torque;
  }

  public void carregar(Double quantidadeCarga) {
    if (quantidadeCarga > this.capacidadeCarga) {
      System.out.println("Capacidade de carga do caminhã insuficiente.");
    } else {
      System.out.println("Carregando caminhão...!");
    }
  }

  @Override
  public void abastecer() {
    System.out.println("Caminhão abastecido!");
  }

  @Override
  public void acelerar() {
    System.out.println("Caminhão acelerado!");
  }

  @Override
  public String toString() {
    return "========= Caminhão ========="
            + "\n"
            + "Marca:'"
            + getMarca()
            + "\n"
            + "Modelo:'"
            + getModelo()
            + "\n"
            + "Cor:'"
            + getCor()
            + "\n"
            + "Preco:"
            + getPreco()
            + "\n"
            + "Capacidade de carga:"
            + capacidadeCarga
            + "\n"
            + "Toque:"
            + torque
            + "\n";
  }
}
