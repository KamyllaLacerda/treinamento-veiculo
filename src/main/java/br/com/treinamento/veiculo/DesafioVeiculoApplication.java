package br.com.treinamento.veiculo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesafioVeiculoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioVeiculoApplication.class, args);
	}

}
